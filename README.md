# pandoc-mutiple-conversor

A bash script for use pandoc multiple times

## Uso básico

Ejecuta `conversor.sh` en el directorio donde estén los archivos a convertir. Si necesitas cambiar el tipo de archivos a convertir, ajusta los valores de `EXT_ENTRADA` `EXT_SALIDA` según el manual de pandoc.