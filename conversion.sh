#!/bin/bash

EXT_ENTRADA="md"
EXT_SALIDA="html"
MD="markdown"

# $ [1,2,3...] Se refiere a los parametros que va recibiendo.
function imprimir_pandoc () {
    $(pandoc -f $1 -t $2 -o $3.$2 $3)
}

function renombrar () {
    $(rename -v ${EXT_ENTRADA}. '' *.${EXT_ENTRADA}.${EXT_SALIDA})
}

if [ $EXT_ENTRADA == 'md' ]
then
    for i in $(ls | egrep "\b.${EXT_ENTRADA}\b$")
    do
	imprimir_pandoc $MD $EXT_SALIDA $i
    done
elif [ $EXT_SALIDA == 'md' ]
then
    for i in $(ls | egrep "\b.${EXT_ENTRADA}\b$")
    do
	imprimir_pandoc $EXT_ENTRADA $MD $i
    done
else
    for i in $(ls | egrep "\b.${EXT_ENTRADA}\b$")
    do
	imprimir_pandoc $EXT_ENTRADA $EXT_SALIDA $i
    done
fi

renombrar

exit 0
